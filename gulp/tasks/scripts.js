'use strict';

const cache = require('gulp-cached');
const gif = require('gulp-if');
const gulp = require('gulp');
const insert = require('gulp-insert');
const jscs = require('gulp-jscs');
const notify = require('gulp-notify');
const sourcemaps = require('gulp-sourcemaps');
const stylish = require('gulp-jscs-stylish');
const uglify = require('gulp-uglify');

gulp.task('lint:js', () => {
	return gulp
		.src(`${SRC_PATH}scripts/**/*.js`)
		.pipe(gif(IS_WATCHING, cache('lint:js')))
		.pipe(stylish());
});

gulp.task('scripts', () => {
	NOTIFY_OPTIONS.title = 'Uglify';
	NOTIFY_OPTIONS.message = 'Le fichier <%= file.relative %> a été compressé';

	return gulp
		.src(`${SRC_PATH}scripts/**/*.js`)
		.pipe(gif(IS_WATCHING, cache('uglify')))
		.pipe(
			insert.transform((contents, file) => {
				if (file.path.endsWith('scripts/app.js')) {
					contents = joinArray(LOGS, 'console.log(', ');') + contents;
				}
				return contents;
			})
		)
		.pipe(sourcemaps.init())
		.pipe(
			uglify({
				compress: {
					drop_console: false,
				},
			}).on('error', err => {
				console.error(err.toString());
				notify.onError({ message: '<%= err.message %>' });
			})
		)
		.pipe(sourcemaps.write('maps'))
		.pipe(
			gulp.dest(function(file) {
				return file.base.replace('/src/', '/dist/');
			})
		)
		.pipe(gif(IS_SERVING, notify(NOTIFY_OPTIONS)));
});

function joinArray(array, before, after) {
	let string = '';
	for (let i = 0; i < array.length; i++) {
		string += before + array[i] + after;
	}
	return string;
}
