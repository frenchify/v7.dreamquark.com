'use strict';

const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync');
const cache = require('gulp-cached');
const cleanCSS = require('gulp-clean-css');
const filter = require('gulp-filter');
const gif = require('gulp-if');
const gulp = require('gulp');
const notify = require('gulp-notify');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const sassInheritance = require('gulp-sass-multi-inheritance');
const scssLint = require('gulp-scss-lint');
const sourcemaps = require('gulp-sourcemaps');
const stylish = require('gulp-scss-lint-stylish2');

// SCSS compilation
gulp.task('styles', () => {
	NOTIFY_OPTIONS.title = 'Sass';
	NOTIFY_OPTIONS.message = 'Le fichier <%= file.relative %> a été mis à jour';

	return (
		gulp
			.src(`${SRC_PATH}styles/**/*.scss`)
			// Filter out unchanged SCSS files
			// if currently watching SCSS changes
			.pipe(gif(IS_WATCHING, cache('scss')))
			// Find files that depend on the files that have changed
			.pipe(sassInheritance({ dir: `${SRC_PATH}styles/` }))
			// Filter out internal imports (folders and files starting with "_" )
			.pipe(
				filter(file => {
					return !/\/_/.test(file.path) || !/^_/.test(file.relative);
				})
			)
			// Begin the fun stuff
			.pipe(sourcemaps.init())
			.pipe(
				sass({
					outputStyle: 'expanded',
				}).on(
					'error',
					notify.onError({
						message: '<%= error.message %>',
					})
				)
			)
			.pipe(postcss([require('postcss-easing-gradients'), autoprefixer()]))
			.pipe(
				cleanCSS({
					level: 1,
				})
			)
			.pipe(sourcemaps.write('map'))
			.pipe(gulp.dest(`${DIST_PATH}styles/`))
			.pipe(browserSync.stream())
			.pipe(gif(IS_SERVING, notify(NOTIFY_OPTIONS)))
	);
});

// SCSS linting
const reporter = stylish();
gulp.task('lint:scss', () => {
	return gulp
		.src(`${SRC_PATH}styles/**/*.scss`)
		.pipe(gif(IS_WATCHING, cache('scssLint')))
		.pipe(
			scssLint({
				maxBuffer: 8000 * 1024,
				customReport: reporter.issues,
			})
		)
		.on(
			'error',
			notify.onError({
				message: '<%= error.message %>',
			})
		);
});
