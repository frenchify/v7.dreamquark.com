'use strict';

const browserSync = require('browser-sync');
const gulp = require('gulp');

gulp.task('setWatch', () => (global.IS_WATCHING = true));
gulp.task('setServe', () => (global.IS_SERVING = true));

gulp.task(
	'serve',
	['setWatch', 'styles', 'scripts', 'lint:js', 'setServe'],
	() => {
		// Init browserSync
		browserSync.init({
			watchTask: true,
			open: false,
			proxy: HOST,
			notify: {
				styles: [
					'z-index: 9999',
					'position: fixed',
					'left: 0',
					'bottom: 0',
					'display: none',
					'padding: 1.5em',
					'margin: 0',
					'font-family: inherit',
					'font-size: 0.9em',
					'font-weight: 400',
					'text-align: center',
					'color: white',
					'background-color: #222',
				],
			},
		});

		// Watch *.tpl and *.js to reload the browser
		gulp
			.watch([`**/*.twig`, `dist/scripts/**/*.js`], { cwd: THEME_PATH })
			.on('change', browserSync.reload);

		// Compile stuff
		gulp.watch([`**/*.scss`], { cwd: `${SRC_PATH}styles/` }, ['styles']);
		gulp.watch([`**/*.js`], { cwd: `${SRC_PATH}scripts/` }, ['scripts']);

		// Lint stuff
		gulp.watch([`**/*.scss`], { cwd: `${SRC_PATH}styles/` }, ['lint:scss']);
		gulp.watch([`**/*.js`], { cwd: `${SRC_PATH}scripts/` }, ['lint:js']);
	}
);
