(function(WILLA, $, window, document) {
	'use strict';

	/** @type {Function} Log alias */
	var log = WILLA.logger.log.bind(window, 'Modal:');

	/** @type {Object} An alias for the document jQuery object */
	var $document = $(document);

	/** @type {Object} Default options */
	var defaults = {
		modal: '#js-modal',
		content: '#js-modal-content',
		openBtns: '.js-modal-open',
		closeBtns: '.js-modal-close',
	};

	/**
	 * Modal class
	 *
	 * @param       {Object} options
	 * @constructor
	 */
	function Modal(options) {
		if (!(this instanceof Modal)) return new Modal(options);
		log('constructor');

		// Extend default options
		options = $.extend(true, defaults, options);
		this.$modal = $(options.modal);
		this.$content = this.$modal.find(options.content);
		this.$closeBtns = this.$modal.find(options.closeBtns);
		this.options = options;

		this.init();

		return this;
	}

	// Register prototyp methods
	Modal.prototype = $.extend(true, Modal.prototype, {
		init: init,
		destroy: destroy,
		open: open,
		close: close,
		keyupHandler: keyupHandler,
		openClickHandler: openClickHandler,
		closeClickHandler: closeClickHandler,
		disableScroll: disableScroll,
		enableScroll: enableScroll,
	});

	/**
	 * Init modal
	 *
	 * @return {Object} The current instance
	 */
	function init() {
		log('init');

		// Bind closing
		this.$closeBtns.on('click', this.closeClickHandler.bind(this));
		$document.on('keyup', this.keyupHandler.bind(this));

		// Bind opening
		$document.on(
			'click',
			this.options.openBtns,
			this.openClickHandler.bind(this)
		);

		return this;
	}

	/**
	 * Destroy the modal
	 */
	function destroy() {
		log('destroy');
		this.$closeBtns.off('click', this.closeClickHandler);
		$document.off('keyup', this.keyupHandler);
		$document.off('click', this.openClickHandler);
	}

	/**
	 * Handler for the keyup event on the document
	 * which will close the modal if the key is escape
	 *
	 * @param  {Object} e The event's object
	 */
	function keyupHandler(e) {
		if (e.which === 27) {
			this.close();
		}
	}

	/**
	 * Handler for the click on modal opening buttons
	 *
	 * @param  {Object} e The event's object
	 */
	function openClickHandler(e) {
		log('openClickHandler');
		var $btn = $(e.currentTarget);
		var options = $btn.data('options');

		// Get and append content
		var content = $(options.contentHolderSelector).html();
		this.$content.append(JSON.parse(content));
		this.open();
	}

	/**
	 * Handler for the click on modal closing butons
	 *
	 * @param  {Object} e The event's object
	 */
	function closeClickHandler(e) {
		log('closeClickHandler');
		this.close();
	}

	/**
	 * Open Modal
	 */
	function open() {
		log('open');
		this.$modal
			.addClass('is-open')
			.attr('aria-hidden', false);

		// Focus on first input if there is some in the content
		this.$content
			.find('input[type="text"]')
			.eq(0)
			.focus();

		this.disableScroll();
	}

	/**
	 * Close modal
	 */
	function close() {
		log('close');
		this.$content.html('');
		this.$modal.removeClass('is-open');
		this.enableScroll();
	}

	/**
	 * Prevent page scroll when modal is open
	 */
	function disableScroll() {
		// Replace the jQuery scrollTop function
		// to avoid unwanted `$(document).scrollTop`
		// when the modal is open
		this.saveScrollTopFn = $.fn.scrollTop;
		$.fn.scrollTop = function() {};
		$('html').css({ overflow: 'hidden' });
	}

	/**
	 * Enable page scroll
	 */
	function enableScroll() {
		$.fn.scrollTop = this.saveScrollTopFn;
		$('html').css({ overflow: '' });
	}


	$(window).on('load', function() {
		WILLA.store.modal = new Modal();
	});

})(WILLA, jQuery, window, document);
