(function($, APP) {

	// What does the accordion plugin do?
	$.fn.accordion = function(options) {

		if (!this.length) { return this; }

		var opts = $.extend(true, {}, $.fn.accordion.defaults, options);
		var $triggers = this;
		var $menu = $triggers.closest('.menu');

		$triggers.each(function() {
			var $this = $(this);
			var $parent = $this.closest('.js-accordion-parent');
			var $container = $parent.find('.js-accordion-container');
			var $content = $parent.find('.js-accordion-content');

			if ($container.length === 0) {
				return;
			}

			$this.data('open', false)
			.on('click', function(e) {
				e.preventDefault();

				var isOpen = $this.data('open');
				var height = isOpen ? 0 : $content.outerHeight();
				var opacity = isOpen ? 0 : 1;

				if (isOpen) {
					$menu.removeClass('menu--has-children-open');
				} else {
					$menu.addClass('menu--has-children-open');
				}

				// Toggle state
				$triggers
					.not($this)
					.data('open', false)
					.removeClass('is-open');
				$this.data('open', !isOpen).toggleClass('is-open');

				// Launch function onStart
				opts.onStart($this, isOpen);
				$container.css({ overflow: '' });

				TweenLite.to($container, 1, { height: height, opacity: opacity, onComplete: function() {
					$container.css({
						height: isOpen ? 0 : 'auto',
						overflow: isOpen ? '' : 'visible',
					});

					// Launch onComplete callback
					opts.onComplete($this, isOpen);
				} });
			});

		});


		return this;
	};

	// default options
	$.fn.accordion.defaults = {
		onStart: function() {},
		onComplete: function() {}
	};

	$('.js-accordion-trigger').accordion();




	// Creates a function for menu and burger icon behavior
	$.fn.burger = function() {

		if (!this.length) { return this; }

		var $this = $(this);
		var $menuContainer = $('.nav-main');
		var $body = $(document.body);
		var isOpenClass = 'is-open';
		var $rows = $('.js-slider-rows');
		var isCollapsed = 'is-collapsed';

		$this.data('open', false);
		$this.removeClass(isOpenClass);
		$menuContainer.removeClass(isOpenClass);

		$this.on('click', function(e) {
			e.preventDefault();

			var isOpen = $this.data('open');



			$this.toggleClass(isOpenClass);
			$menuContainer.toggleClass(isOpenClass);
			$rows.toggleClass(isCollapsed);
			$this.data('open', !isOpen);

			$(APP).trigger('menu:toggle', [!isOpen]);
		});

		return this;
	};

	$('.js-burger-trigger').burger();

})(jQuery, APP);
