(function($, APP, window, document) {

	var view = APP.store.view;

	// default options
	var defaults = {
		onStart: function() {},
		onComplete: function() {}
	};

	function SliderGallery(selector, options) {
		if (!(this instanceof SliderGallery)) return new SliderGallery(selector, options);

		this.opts = $.extend(true, {}, defaults, options);
		this.$rows = $(selector);
		this.$parent = $('.section-home__rows');
		this.timelines = [];
		this.rowInners = [];
		this.$figures = $();
		this.mousewheelTimer = null;

		this.setRowInnerPosition = this.setRowInnerPosition.bind(this);
		this.mousewheelHandler = this.mousewheelHandler.bind(this);
		this.menuOpeningdHandler = this.menuOpeningdHandler.bind(this);

		this.init();
	}



	SliderGallery.prototype.init = function() {
		var self = this;

		self.$rows.each(function(index, row) {
			var $row = $(row);
			var animationTime = parseInt($row.attr('data-animation-time'));
			var isReverse = ($row.attr('data-orientation') == 'reverse') ? true : false;

			var $rowInner = $row.find('.row__inner');
			var $rowInnerClone = $rowInner.clone();
			$rowInnerClone.addClass('row__inner--clone');

			self.rowInners.push({
				isReverse: isReverse,
				$element: $rowInnerClone,
			});

			if (isReverse) {
				$rowInnerClone.insertBefore($rowInner);
			} else {
				$rowInnerClone.insertAfter($rowInner);
			}

			var tl = self.createTimeline([$rowInner, $rowInnerClone], animationTime, isReverse);
			self.timelines.push(tl);

		});

		this.$figures = this.$parent.find('figure');

		self.setRowInnerPosition();
		view.onResizeend.setRowInnerPosition = self.setRowInnerPosition;

		$(window).on('mousewheel', self.mousewheelHandler);

		self.menuOpeningdHandler();
	}



	SliderGallery.prototype.isInViewport = function(figure) {
		var rect = figure.getBoundingClientRect();

		return (
			rect.left >= 0 && rect.right <= (window.innerWidth || document. documentElement.clientWidth) ||
			rect.left < 0 && rect.right >= 0 ||
			rect.left >= 0 && rect.left < (window.innerWidth || document. documentElement.clientWidth)
		);
	}



	SliderGallery.prototype.menuOpeningdHandler = function() {
		var self = this

		$(APP).on('menu:toggle', function(e, isOpen) {

			if (isOpen) {
				$.each(self.timelines, function(index, tl) {
					tl.pause();
				});

				self.$figures.each(function(index, figure) {
					var isInViewport = self.isInViewport(figure);

					if (isInViewport) {

						var sizes = figure.getBoundingClientRect();
						var width = sizes.width;
						var distanceToRight = sizes.right;

						var x = 0;
						var formule = window.innerWidth - distanceToRight + (width / 2);

						if(distanceToRight >= 1500) {
							x = formule + 100;
						} else if (distanceToRight <= 1499 && distanceToRight >= 800) {
							x = formule + 50;
						} else {
							x = formule;
						}

						TweenMax.to(figure, 1.5, {
							x: x,
						});
					}
				});

			} else {
				TweenMax.to(self.$figures, 1.5, {
					x: 0,
				})
				$.each(self.timelines, function(index, tl) {
					tl.play();
				});
			}



		});
	}



	SliderGallery.prototype.mousewheelHandler = function(e) {
		if ($('.js-burger-trigger').data('open')) {
			return false
		}

		var self = this;
		var deltaX = e.deltaX;
		var deltaY = e.deltaY;

		var progress = (deltaY + deltaX) / 5000;

		if (Math.abs(progress) > 0.001) {

			$.each(self.timelines, function(index, timeline) {
				timeline.pause();
			});

			TweenMax.set(self.timelines, {
				progress: '+=' + progress,
			});
		} else {
			$.each(self.timelines, function(index, timeline) {
					// timeline.play();
			});
		}

		clearTimeout(self.mousewheelTimer);
		self.mousewheelTimer = setTimeout(function() {
			$.each(self.timelines, function(index, timeline) {
					// timeline.play();
			});
		}, 100);
	}



	SliderGallery.prototype.setRowInnerPosition = function() {
		$.each(this.rowInners, function(index, rowInner) {
			var width = rowInner.$element.outerWidth();
			if (rowInner.isReverse) {
				rowInner.$element.css({ left: -width });
			} else {
				rowInner.$element.css({ left: width });
			}
		});
	}


	/**
	 * Create a timeline to animate the home slider
	 *
	 * @param Array rowInner The row inner elements to transform
	 * @param Integer duration The duration of the animation
	 * @param Boolean isReverse The direction of the animation
	 * @return TimelineMax The created timeline
	 */

	SliderGallery.prototype.createTimeline = function(rowInners, duration, isReverse) {

		var duration;
		duration = (typeof duration === 'number') ? duration : 50;
		isReverse = (typeof isReverse === 'boolean' && isReverse) ? true : false;

		var tl = new TimelineMax({
			repeat: -1,
			paused: true, //debug
		});

		if (isReverse) {
			rowInners = rowInners.reverse();
		}

		tl.fromTo(rowInners, duration, {
			x: '0%'
		}, {
			x: (!isReverse) ? '-100%' : '100%',
			ease: Linear.easeNone,
		});

		return tl;
	}


	$(window).on('load', function() {
		new SliderGallery('.js-slider-rows');
	});

})(jQuery, APP, window, document);
