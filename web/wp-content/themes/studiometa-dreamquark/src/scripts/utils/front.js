$(function () {

    $('#partners-slider').owlCarousel({
        loop: true,
        margin: 30,
        dots: false,
        nav: false,
        slideSpeed: 1500,
        autoplay: false,
        smartSpeed: 700,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                dots: true,
                dragEndSpeed: 4
            },
            768: {
                items: 3,
                dots: true
            },
            992: {
                items: 4
            }
        },
        onInitialize: owlPartnersInit,
        onResize: owlPartnersResize
    });

    function owlPartnersInit(e) {
        nb_items = $('#partners-slider .item-holder').length;

        if (window.innerWidth > 991 && nb_items >= 4) {
            e.relatedTarget.options.mouseDrag = false;
            e.relatedTarget.options.touchDrag = false;
            e.relatedTarget.settings.mouseDrag = false;
            e.relatedTarget.settings.touchDrag = false;
        }
    }

    function owlPartnersResize(e) {
        nb_items = $('#partners-slider .owl-item.active').length;

        if (window.innerWidth > 991 && nb_items >= 4) {
            if (e.relatedTarget.options.mouseDrag == true) {
                $('#partners-slider').trigger('destroy.owl.carousel');
                $('#partners-slider').owlCarousel({
                    loop: true,
                    margin: 30,
                    dots: false,
                    nav: false,
                    mouseDrag: false,
                    touchDrag: false,
                    slideSpeed: 1500,
                    autoplay: false,
                    smartSpeed: 700,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        992: {
                            items: 4
                        }
                    },
                    onInitialize: owlPartnersInit,
                    onResize: owlPartnersResize
                });

            }
        } else {
            if (e.relatedTarget.options.mouseDrag == false) {
                $('#partners-slider').trigger('destroy.owl.carousel');
                $('#partners-slider').owlCarousel({
                    loop: true,
                    margin: 30,
                    dots: false,
                    nav: false,
                    mouseDrag: true,
                    touchDrag: true,
                    slideSpeed: 1500,
                    autoplay: false,
                    smartSpeed: 700,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 2
                        },
                        768: {
                            items: 3
                        },
                        992: {
                            items: 4
                        }
                    },
                    onInitialize: owlPartnersInit,
                    onResize: owlPartnersResize
                });
            }
        }
    }

    $('.usecases-slider').owlCarousel({
        margin: 30,
        dots: true,
        nav: false,
        rewind: true,
        slideSpeed: 1500,
        autoplay: false,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        smartSpeed: 700,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });


    $('.awards-slider-1').bxSlider({
        minSlides: 1,
        maxSlides: 6,
        slideWidth: 189,
        slideMargin: 0,
        ticker: true,
        speed: 27000,
        responsive: false
    });

    $('.awards-slider-2').bxSlider({
        autoDirection: 'prev',
        minSlides: 1,
        maxSlides: 6,
        slideWidth: 189,
        slideMargin: 0,
        ticker: true,
        speed: 20000,
        responsive: false
    });

    $('.awards-slider-3').bxSlider({
        minSlides: 1,
        maxSlides: 6,
        slideWidth: 189,
        slideMargin: 0,
        ticker: true,
        speed: 11000,
        responsive: false
    });

    $('.slider-news').owlCarousel({
        loop: false,
        margin: 30,
        dots: true,
        nav: false,
        items:1,
        autoplay: false,
        mouseDrag: false,
        touchDrag: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        navText: ["<img class='w-50 img-fluid mr-5' style='transform: rotate(180deg); width: 20px !important' src='./wp-content/themes/studiometa-dreamquark/assets/svg/layout/arrow-grey.svg' />", "<img class='w-50 img-fluid ml-5' style='width: 20px !important' src='./wp-content/themes/studiometa-dreamquark/assets/svg/layout/arrow-grey.svg' />"],
        responsive: {
            0: {
                nav: true
            },
            992: {
                nav: false
            }
        }
    });

    $('.slider-otherstories').owlCarousel({
        loop: false,
        margin: 30,
        dots: true,
        nav: false,
        items:1,
        autoplay: false,
        mouseDrag: false,
        touchDrag: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
        }
    });

    $('.testimonials-slider-2').owlCarousel({
        loop: true,
        margin: 30,
        dots: false,
        nav: true,
        smartSpeed: 700,
        slideSpeed: 1500,
        autoplayTimeout: 3000,
        autoplay: true,
        responsiveRefreshRate: 200,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
                dots: true
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
            },
            1200: {
                items: 4,
            }
        }
    });

    $(".roi-slider").owlCarousel({
        loop:true,
        margin:30,
        dots:true,
        nav:false,
        slideSpeed:1500,
        smartSpeed:700,
        items: 3,
        mouseDrag: false,
        touchDrag: false,
        responsive: {
            0: {
                items: 1,
                mouseDrag: true,
                touchDrag: true,
                autoplay: true,
                autoplayHoverPause: true,
            },
            768: {
                items: 2,
                mouseDrag: true,
                touchDrag: true,
                autoplay: true,
                autoplayHoverPause: true,
            },
            992: {
                items: 3,
            }
        }
    });

    $(".beliefs-slider").owlCarousel({
        loop:false,
        margin:30,
        dots:false,
        nav:false,
        slideSpeed:1500,
        autoplay:0,
        smartSpeed:700,
        responsive:{
            0:{
                items:1,
                dots:true,
                mouseDrag:true,
                touchDrag:true,
            },
            768:{
                items:2,
                dots:true,
                mouseDrag:true,
                touchDrag:true,
            },
            992:{
                items:3,
                dots:true,
                mouseDrag:true,
                touchDrag:true,
            },
            1200:{
                items:4,
                dots:false,
                mouseDrag:false,
                touchDrag:false
            }
        }
    });

    $('.owl-carousel-team').owlCarousel({
        margin: 30,
        loop: true,
        dots: false,
        autoWidth: true,
        items: 1,
        slideSpeed: 1500,
        center: true,
        rewind: false,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        nav: true,
        smartSpeed: 1500,
        responsiveClass: true,
        onInitialized: owlTeamInit,
        navText: ["<img class='w-50 img-fluid mt-3 mb-5 mr-lg-5' style='transform: rotate(180deg); width: 20px !important' src='./wp-content/themes/studiometa-dreamquark/assets/svg/layout/arrow.svg' /> <span id='current_member'></span>", "<img class='w-50 img-fluid mt-3 mb-5 ml-lg-5' style='width: 20px !important' src='./wp-content/themes/studiometa-dreamquark/assets/svg/layout/arrow.svg' />"]
    });

    $('.owl-carousel-team').mouseleave(function() { $(this).trigger('next.owl.carousel'); });

    function set_current_member(index) {
        member = $('#team .owl-item').eq(index).find('.team-member');
        $("#current_member").html(member.html());
    }

    // Listen to owl events:
    $('.owl-carousel-team').on('changed.owl.carousel', function (event) {
        set_current_member(event.property.value);
    });

    function owlTeamInit(event) {
        member = $('#team .owl-item.center .team-member');
        $("#current_member").html(member.html());
    }

    // ------------------------------------------------------- //
    // Scroll Top Button
    // ------------------------------------------------------- //
    $('#scrollTop').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    var c, currentScrollTop = 0,
        navbar = $('.navbar');
    container = $('header.fixed-top>div');
    navbar_nav = $('.navbar-nav');
    get_demo_menu = $('#get_demo_menu');
    scrollNotOk = $('.scrollNotOk');
    scrollOk = $('.scrollOk');

    $(window).on('scroll', function (e) {
        if (!$('body').hasClass('lock')) {
            if ($(window).scrollTop() <= 120) {
                $(".KW_progressContainer").addClass('d-none');
                $(".KW_progressContainer").removeClass('d-visible');
                if ($('body').hasClass('home')) {

                    if ($('#navbarSupportedContentScroll').is(':visible')) {
                        $('header.fixed-top').removeClass('navbar-scroll');
                    } else {
                        $('header.fixed-top').removeClass('navbar-scroll');
                    }
                }
            } else {
                $(".KW_progressContainer").addClass('d-visible');
                $(".KW_progressContainer").removeClass('d-none');
                if ($('body').hasClass('home')) {

                    if ($('#navbarSupportedContentNoScroll').is(':visible')) {
                        $('header.fixed-top').addClass('navbar-scroll');
                    } else {
                        $('header.fixed-top').addClass('navbar-scroll');
                    }
                }
            }
        }
    });

    if ($(window).scrollTop() <= 120) {
        if ($('body').hasClass('home')) {
            $('header.fixed-top').removeClass('navbar-scroll');
        }
        $(".KW_progressContainer").addClass('d-none');
        $(".KW_progressContainer").removeClass('d-visible');


    } else {
        if ($('body').hasClass('home')) {
            $('header.fixed-top').addClass('navbar-scroll');
        }
        $(".KW_progressContainer").addClass('d-visible');
        $(".KW_progressContainer").removeClass('d-none');

    }

    // ---------------------------------------------------------- //
    // Preventing URL update on navigation link click
    // ---------------------------------------------------------- //
    $('.link-scroll').on('click', function (e) {

        if ($('.navbar-toggler').hasClass('active')) {
            $('body').removeClass('lock');
            window.scrollTo(0, lastScrollPos);
            $('#navbarSupportedContentScroll').removeClass('active');
            $('#navbarSupportedContentNoScroll').removeClass('active');
            setTimeout(function () {
                $('#navbarSupportedContentScroll').removeClass('top');
                $('#navbarSupportedContentNoScroll').removeClass('top');
                $('body').css('overflow', 'auto');
            }, 700);
            $('.navbar-toggler').removeClass('active');
            $('header.header > nav.navbar').removeClass('blue');
        }

        var anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $(anchor.attr('href')).offset().top - 100
        }, 1000);
        e.preventDefault();
    });


    // ---------------------------------------------------------- //
    // Scroll Spy
    // ---------------------------------------------------------- //
    $('body').scrollspy({
        target: '#navbarSupportedContent',
        offset: 80
    });



    // ------------------------------------------------------ //
    // For demo purposes, can be deleted
    // ------------------------------------------------------ //


    var setSharpenDecisions = function () {
        var sharpen_decisions = $("#sharpen_decisions");
        var height = sharpen_decisions.find('.real-content').outerHeight();
        $('#industries .bank-insurance').css('margin-left', '0px');

        if (window.innerWidth > 991) {
            difference_between_sharpen = $('#sharpen_decisions .fake-row-background-second').offset().left - $('#industries .bank-insurance').offset().left - 15;
        } else {
            difference_between_sharpen = 0;
        }

        sharpen_decisions.find('.fake-row-background').css('height', height+'px');
        sharpen_decisions.find('.real-content').css('margin-top', '-'+height+'px');
        $('#industries .bank-insurance').css('margin-left', difference_between_sharpen+'px');
    }

    var createBrainCarousel = function () {
        if (window.innerWidth < 992) {
            if (!$(".brain-slider").hasClass('owl-carousel')) {
                $('.brain-slider').addClass('owl-carousel');
                $(".brain-slider").owlCarousel({
                    loop: true,
                    margin: 0,
                    dots: true,
                    nav: false,
                    slideSpeed: 1500,
                    smartSpeed: 700,
                    items: 1,
                    mouseDrag: true,
                    touchDrag: true,
                    autoplay: true,
                    autoplayHoverPause: true,
                });
            }
        } else {
            if ($('.brain-slider').hasClass('owl-loaded')) {
                $(".brain-slider").trigger('destroy.owl.carousel');
                $(".brain-slider").removeClass('owl-carousel');
            }
        }
    }
;
    if ($('body').hasClass('home')) {
        setSharpenDecisions();
        createBrainCarousel();
    }

    $(window).resize(function () {
        if ($('body').hasClass('home') && !$('body').hasClass('lock')) {
            setSharpenDecisions();
            createBrainCarousel();

            $('#navbarSupportedContentScroll').height('auto');
            $('#navbarSupportedContentNoScroll').height('auto');
            $('body').css('overflow', 'auto');
        }
    });

    // Wording on Team block
    var wordSources = $(".selected_word_hidden").map(function() {
        return $(this).text();
     }).get();

     // Prepare recursion
    replaceWord = function(word_target){
        $('#text_tochange').fadeOut(500, function() {
            $(this).text(word_target).fadeIn(500);
        });
    }
    var cnt_t = 1;
    setInterval(function(){
        if (cnt_t >= wordSources.length) {
            cnt_t = 0;
        }
        wordTarget = wordSources[cnt_t];
        replaceWord(wordTarget);
        cnt_t++;
    }, 2000);
    $(window).scroll(function() {
        var wintop = $(window).scrollTop(),
        docheight = $('main').outerHeight(),
        winheight = $(window).height();
        var totalScroll = (wintop/(docheight-winheight))*100;
        $(".KW_progressBar").css("width",totalScroll+"%");
      });

    $(document).on("gform_confirmation_loaded", function (e, form_id) {
        if (form_id == 1) {
            $('#demo-form .left-part').remove();
            $('#demo-form .right-part').removeClass('col-md-8').addClass('col-md-12');
            $('#demo-form').css('width', '500px');
        } else if (form_id == 3) {
            $('#join-team .left-part').remove();
            $('#join-team .right-part').removeClass('col-md-8').addClass('col-md-12');
            $('#join-team').css('width', '500px');
        }
    });


    $(document).on('click', '.fancybox', function() {
        var div_pointed = $(this).data('src');
        $.fancybox.open({
            src  : div_pointed,
            type : 'inline',
            opts : {
                beforeClose: function () {
                    location.reload();
                    return false;
                }
            }
        });
        return false;
    });

    function windowPopup(url, width, height) {
        // Calculate the position of the popup so
        // it’s centered on the screen.
        var left = (screen.width / 2) - (width / 2),
            top = (screen.height / 2) - (height / 2);

        window.open(
            url,
            "",
            "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left
        );
    }

    $(document).on('click', '.sharing-button-wrapper a', function() {
        windowPopup($(this).attr("href"), 600, 400);

       return false;
    });

    $('section.extra-features .item').on('click', function (e) {
        $('section.extra-features #laptop-container .laptop-bg>.laptop-content-bg.show').removeClass('show');

        $('section.extra-features #laptop-container '+$(this).attr('href')).addClass('show');

        return false;
    });

    $('section.extra-features .item').on('hover', function (e) {
        $('section.extra-features #laptop-container .laptop-bg>.laptop-content-bg.show').removeClass('show');

        $('section.extra-features #laptop-container '+$(this).attr('href')).addClass('show');

        return false;
    });

    $(document).on('click', '#team .open-overlay', function(e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).next().show();

        if (!$(this).closest('.owl-item').hasClass('center')) {
            $('.owl-carousel-team').trigger('prev.owl.carousel');
        }

        return false;
    });

    $(document).on('click', '#team .close-overlay', function(e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).closest('.card-img-overlay').fadeToggle();
    });

    $(document).on('click', '#use_cases .box .close-overlay', function(e) {
        $(this).closest('.box').removeClass('hover');
        return false;
    });

    $(document).on('click', '#use_cases .box', function(e) {
        if ($(window).width() < 992) {
            $(this).closest('.box').addClass('hover');
        }
        return false;
    });

    var lastScrollPos = 0;

    $('.navbar-toggler').on('click', function() {

        var href = $(this).data('target');
        var target = $(href);

        if ($(this).hasClass('active')) {
            target.removeClass('active');
            $('body').removeClass('lock');
            window.scrollTo(0, lastScrollPos);
            setTimeout(function(){
                target.removeClass('top');
            }, 700);
            $(this).removeClass('active');
            $('header.header > nav.navbar').removeClass('blue');
        } else {
            lastScrollPos = $(window).scrollTop();
            $(this).addClass('active');
            target.addClass('active').addClass('top');
            target.height($(window).height() - 103);
            $('header.header > nav.navbar').addClass('blue');
            $('body').addClass('lock');
        }

        return false;

    });


    $('[data-toggle="tooltip"]').tooltip();
});