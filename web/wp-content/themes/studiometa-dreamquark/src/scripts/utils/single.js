$(function () {
    var sharingbtn = $("#sharing-buttons");
    var sticky = parseInt(sharingbtn.offset().top - 87);
    var sticky_width = sharingbtn.width();

    check_sticky();

    $(window).on('scroll', function () {
        check_sticky();
    });

    $(window).on('resize', function () {
        check_sticky();
    });

    function check_sticky() {
        if ($(window).width() >= 768) {
            if ($(window).scrollTop() >= sticky) {
                sharingbtn.addClass("sticky");
                width_col = sharingbtn.parent().width();
                sharingbtn.css('margin-left', parseInt((width_col/2) - (sticky_width/2))+'px');
            } else {
                sharingbtn.removeClass("sticky").css('margin-left', 'auto');
            }
        }
    }
});