<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

require_once('TwitterAPIExchange.php');

$context = Timber::get_context();

// Get Articles (Latest News, What's News ?)
$args = array(
    'post_type'      => array( 'post' ),
    'posts_per_page' => '10', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date',
);
$context['articles'] = Timber::get_posts( $args );

// TWITTER
$file = getcwd()."/wp-content/cache/apis/twitter.json";

if (!file_exists($file) || (time() - filemtime($file)) > 3600) {

	$settings = array(
		'oauth_access_token' => "2851159222-wSbLiv2xaVERLqfoQQLvWZRXSd4ZSg4XPYMv88d",
		'oauth_access_token_secret' => "5TJ6xqHydERA8IgMqopXQLEnhTJiPLNfEdVlV7RXx0O5q",
		'consumer_key' => "mY75FMujEUGm8iVnC4DIyckc4",
		'consumer_secret' => "B9GMeNmLkUMuLurwp4R5MKiooidOCdylJJQHzXgfMG6goh3GpX"
	);

	$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
	$requestMethod = 'GET';
	$getfield = '?count=10';
	$twitter = new TwitterAPIExchange($settings);
	$tweets = $twitter->setGetfield($getfield)
					  ->buildOauth($url, $requestMethod)
					  ->performRequest();

	$twitter_file = fopen( $file, "w" );
	fwrite( $twitter_file, $tweets);
	fclose( $twitter_file );

	$context['tweets'] = json_decode($tweets);
} else {
	$context['tweets'] = json_decode(file_get_contents($file));
}


// Get Page Builder Informations
$homepage = get_option( 'page_on_front' );
$args = array(
    'post_type'      => array( 'page' ),
    'posts_per_page' => '1', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date',
);

$context['builder'] = Timber::get_post( $homepage );

// Get Team Member Informations
$args = array(
    'post_type'      => array( 'dq_team' ),
    'posts_per_page' => '100000000000000', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date',
);
$context['team_members'] = Timber::get_posts( $args );

// Get Use Cases
$args = array(
    'post_type'      => array( 'dq_use_case' ),
    'posts_per_page' => '10', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date',
);
$context['use_cases'] = Timber::get_posts( $args );

$context['site_name'] = get_bloginfo('name');
$context['site_description'] = get_bloginfo('description');
$custom_logo_id = get_theme_mod( 'custom_logo' );
$context['logo_url'] = wp_get_attachment_image_src( $custom_logo_id , 'full' )[0];
$templates = array( 'front-page.twig' );
Timber::render( $templates, $context );