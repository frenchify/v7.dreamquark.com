<?php
// Composer
require_once(__DIR__ . '/vendor/autoload.php');

// Timber
$timber = new \Timber\Timber();

if (! class_exists('Timber')) {
    add_action( 'admin_notices', function() {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
    });

    // Load error template if timber isn't installed
    add_filter( 'template_include', function($template) {
        return __DIR__ . '/no-timber.html';
    });

    return;
}



Timber::$dirname = array( 'templates', 'views' );

class StarterSite extends TimberSite {

    function __construct() {

        // Which Plugin this Theme use ?
        // true/false those constant to load theme-support & utility fn's for those plugins
        define('THEME_USE_ACF', true);
        define('THEME_USE_YOAST', true);
        define('THEME_USE_WOOCOMMERCE', false);
        define('THEME_USE_WPML', false);
        define('THEME_USE_GRAVITYFORMS', true);
        define('GMAP_API_KEY', 'AIzaSyD5w1vYBsVotjLbPSGMnnOKmzFA-o465oo');

        // Timber Context
        add_filter( 'timber_context', array( $this, 'add_to_context' ) );

        // Add functions to Twig
        add_filter( 'get_twig', array( $this, 'add_to_twig' ) );

        // Clean Up
        add_action( 'after_setup_theme', array( $this, 'studiometa_start_cleanup' ) );

        // Theme Support
        add_action( 'after_setup_theme', array( $this, 'theme_support' ) );

        add_theme_support( 'custom-logo' );

        // Registger custom post types
        add_action( 'init', array( $this, 'register_post_types' ) );

        // Register custom Taxonomies
        add_action( 'init', array( $this, 'register_taxonomies' ) );

        // Register Custom Sidebars
        add_action( 'init', array( $this, 'register_sidebars' ) );

        // Register Custom Menus
        add_action( 'init', array( $this, 'register_menus' ) );

        // Menu Utils functions
        add_action( 'init', array( $this, 'studiometa_menus_utils' ) );

	    add_action( 'init', 'disable_wp_emojicons' );

	    function disable_wp_emojicons()
	    {
		    // all actions related to emojis
		    remove_action( 'admin_print_styles', 'print_emoji_styles' );
		    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		    remove_action( 'wp_print_styles', 'print_emoji_styles' );
		    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	    }

	    function studiometa_mime_types($mimes) {
		    $mimes['svg'] = 'image/svg+xml';
		    return $mimes;
	    }
	    add_filter('upload_mimes', 'studiometa_mime_types');

        // Utils ACF Functions
        if (THEME_USE_ACF) {
            add_action( 'init', array( $this, 'studiometa_acf_utils' ) );
        }

        // Utils Woocommerce Functions
        if (THEME_USE_WOOCOMMERCE) {
            add_action( 'init', array( $this, 'studiometa_woocommerce_utils' ) );
        }


        // ES Géothermie Utils
        add_action('init', array( $this, 'studiometa_utils' ));

        // Remove Gravity Forms Stylesheet
        add_filter('pre_option_rg_gforms_disable_css', '__return_true');

        // Init Gravity Forms scripts in footer
        add_filter( 'gform_init_scripts_footer', '__return_true' );

        // Hide admin bar
        add_filter('show_admin_bar', '__return_false');

        // Custom login
        add_action('login_head', array( $this, 'custom_login_style' ) );

        add_filter('admin_footer_text', array($this, 'remove_footer_admin') );

        if (!is_admin()) {
            add_filter('script_loader_tag', array($this, 'add_defer_attribute'), 10, 2);
            add_action( 'wp_enqueue_scripts', array($this, 'register_scripts'), 1, 1 );
            add_action( 'wp_enqueue_scripts', array($this, 'register_styles'), 1, 1 );
        }

        // Stop TinyMCE removing tags in wysiwyg

        add_filter('tiny_mce_before_init', array($this,'override_mce_options') );



        parent::__construct();
    }



    /* ================================================================
     * 1. Timber
     * Add data to context
     * Add functions to twig
     * ================================================================ */
    /**
     * 	add_to_context
     * 	These values are available everytime you call Timber::get_context();
     */
    function add_to_context($context) {
        $context['WP_ENV'] = WP_ENV;
        $context['site'] = $this;
        $context['menu'] = new TimberMenu( 'Main menu' );
        $context['template_directory'] = get_template_directory();
        $context['template_directory_uri'] = get_template_directory_uri();

        // Images de layout
        $context['img_layout'] = get_template_directory_uri().'/assets/img/layout/';

        if (THEME_USE_WPML) {
            $context['current_language'] = ICL_LANGUAGE_CODE;
        }

        // Detect if IE for sprite SVG
        // Sprite SVG
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $ie   = preg_match('/(?i)msie/', $user_agent);
        $ie11 = preg_match('/Trident.*rv[ :]*11\./', $user_agent);
        $edge = preg_match('/Edge\/./', $user_agent);

        if ( $ie || $ie11 || $edge ) {
            $context['sprite_body'] = './assets/svg/sprite.svg';
            $context['sprite_href'] = '';
            $context['ie'] = true;
        } else {
            $context['sprite_href'] = get_template_directory_uri().'/assets/svg/sprite.svg';
        }

        // Breadcrumbs
        if (function_exists('yoast_breadcrumb') && THEME_USE_YOAST) {
            $context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="breadcrumbs">','</nav>', false );
        }

        return $context;
    }

    /**
     * Add functions to twig
     * When called by Twig, the PHP callable receives the left side of
     * the filter (before the pipe |) as the first argument and the extra
     * arguments passed to the filter (within parentheses ()) as extra arguments.
     * Use it like this in a *.twig file : {{ $arg1|function($arg2, $arg3 ...) }}
     */
    function add_to_twig($twig) {
        /* Add old_or_young function to twig - use {{ 30|old_or_young(20) }} */
        $twig->addFilter('old_or_young', new Twig_SimpleFilter('old_or_young', array($this, 'old_or_young')));
        $twig->addFilter('get_filesize', new Twig_SimpleFilter('get_filesize', array($this, 'get_filesize')));
        return $twig;
    }

    /**
     * Enqueue a custom stylesheet
     * Usage :
     * {% do action('studiometa_enqueue_scripts', 'app.js') %}
     * @param  String $handle The handle for the registered script
     * @return Void
     */
    function studiometa_enqueue_scripts($handle)
    {
        wp_enqueue_script($handle);
    }



    /* ================================================================
     * 2. Clean up functions
     * Remove & clean all the stuff added by wordpress
     * ================================================================ */
    /**
     * [studiometa_start_cleanup description]
     */
    function studiometa_start_cleanup() {
        require_once( 'lib/clean-up.php' );
    }



    /* ================================================================
     * 3: Register functions
     * Menus / Post types / Taxonomies / Sidebars
     * ================================================================ */
    /**
     * Define what our theme support
     */
    function theme_support() {
        require_once( 'lib/theme-support.php' );
    }

    /**
     * Register custom post types
     * Doc : https://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_post_types() {
        require_once( 'lib/post-types.php' );
    }

    /**
     * Register custom taxonomies
     * Doc : https://codex.wordpress.org/Function_Reference/register_taxonomy
     */
    function register_taxonomies() {
        require_once( 'lib/taxonomies.php' );
    }

    /**
     * Register custom menus
     * Doc : https://codex.wordpress.org/Function_Reference/register_nav_menus
     */
    function register_menus() {
        require_once( 'lib/menus.php' );
    }

    /**
     * Register custom sidebars
     * Doc : https://codex.wordpress.org/Function_Reference/register_sidebar
     */
    function register_sidebars() {
        require_once( 'lib/sidebars.php' );
    }

    /**
     * Register custom scripts
     * Doc : https://codex.wordpress.org/Function_Reference/register_sidebar
     */
    function register_scripts() {
        require_once( 'lib/scripts.php' );
    }

    /**
     * Register custom styles
     * Doc : https://codex.wordpress.org/Function_Reference/register_sidebar
     */
    function register_styles() {
        require_once( 'lib/styles.php' );
    }

    /* ================================================================
     * 4: Utility functions
     * To improve wordpress
     * ================================================================ */
    /**
     * Custom functions for menu
     */
    function studiometa_menus_utils() {
        require_once( 'lib/menus-utils.php' );
    }

    /**
     * Custom functions for acf goes here
     */
    function studiometa_acf_utils() {
        require_once( 'lib/acf-utils.php' );
    }

    /**
     * Custom functions for woocommerce
     */
    function studiometa_woocommerce_utils() {
        require_once( 'lib/woocommerce-utils.php' );
    }

    /**
     * Load custom style for login page
     */
    function custom_login_style() {
        echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/styles.css" />';
    }

    /**
     * Load custom hooks for es geothermal
     */

    function studiometa_utils() {
        require_once('lib/dreamquark-hooks.php');
    }



    /* ================================
     * 5. Custom Twig filters
     * ================================ */
    /**
     * Example de function custom
     * you can use it like that in every twig file
     * {{ 30|old_or_young(20) }}
     */
    function old_or_young($a, $b) {
        return $a > $b ? 'old' : 'young';
    }

    function get_filesize($url) {
        return filesize($url);
    }



    /* ================================
     * 5. Custom functions relative to project
     * Use require_once('lib/custom/xxx')
     * ================================ */

    /**
     * [example description]
     * @return [type] [description]
     */
    function example() {
        require_once('lib/custom/example.php');
    }

    // Studiometa footer in admin panel

    function remove_footer_admin () {
        echo 'Made with ❤️ by <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="69" height="11" viewBox="0 0 69 11" enable-background="new 0 0 69 11" xml:space="preserve"><path d="M3.2 9.4c-0.9 0-1.7-0.3-2.5-0.7L0 10.1C0.9 10.7 2.1 11 3.1 11c2.3 0 3.8-1.2 3.8-3.1 0-1.3-0.8-2.3-2.3-2.7L3.4 4.9c-0.8-0.2-1-0.4-1-0.9 0-0.6 0.5-1 1.3-1 0.7 0 1.3 0.2 2.1 0.7l0.9-1.3c-0.8-0.6-1.9-0.9-3-0.9 -1.9 0-3.2 1.1-3.2 2.8 0 0.5 0.1 1 0.4 1.4 0.3 0.6 0.9 0.9 1.8 1.1l1 0.3c0.8 0.2 1.2 0.6 1.2 1.2C4.8 9 4.2 9.4 3.2 9.4M11.5 9.6c-0.3 0.1-0.5 0.1-0.6 0.1 -0.6 0-0.8-0.2-0.8-1.1V5.3h1.1l0.4-1.2h-1.6V2H8.4v2H7.8v1.2h0.7v3.6c0 0.6 0 0.8 0.1 1 0.2 0.6 0.9 1 1.8 1 0.4 0 0.9-0.1 1.4-0.3L11.5 9.6zM18.1 9V4h-1.7V9c-0.1 0.3-0.7 0.7-1.1 0.7 -0.3 0-0.5-0.1-0.6-0.3 -0.1-0.2-0.1-0.5-0.1-1.1V4.1h-1.7v4.5c0 0.7 0 1 0.1 1.3 0.2 0.7 1 1.1 2 1.1 0.7 0 1.3-0.2 1.8-0.7 0.1 0.3 0.3 0.5 0.5 0.7l1.2-0.7C18.2 9.9 18.1 9.5 18.1 9M23.4 9c-0.2 0.2-0.6 0.4-0.9 0.4 -0.9 0-1.2-0.5-1.2-1.9 0-1.5 0.3-2.1 1.1-2.1 0.3 0 0.6 0.1 1 0.4L23.4 9C23.4 9 23.4 9 23.4 9zM23.8 10.8h1.6c-0.1-0.3-0.2-0.8-0.2-2.5V1.1h-1.7v2.3c0 0.4 0 0.9 0 1C23.1 4.1 22.7 4 22.2 4c-1.7 0-2.9 1.5-2.9 3.5s1.1 3.4 2.8 3.4c0.6 0 1.1-0.2 1.6-0.6C23.7 10.6 23.7 10.7 23.8 10.8"/><rect x="26.9" y="3.9" width="1.8" height="6.8"/><path d="M27.8 0c-0.6 0-1.1 0.5-1.1 1.1s0.5 1.1 1.1 1.1 1.1-0.5 1.1-1.1C28.8 0.5 28.4 0 27.8 0"/><path d="M33 9.7c-0.3 0-0.7-0.2-0.9-0.6 -0.1-0.3-0.2-0.9-0.2-1.7 0-0.7 0.1-1.1 0.2-1.5 0.1-0.4 0.5-0.7 0.9-0.7 0.3 0 0.6 0.1 0.8 0.3 0.2 0.3 0.3 0.9 0.3 1.8C34.1 9 33.8 9.7 33 9.7M35.2 4.9c-0.6-0.7-1.3-1-2.3-1 -1.8 0-3 1.4-3 3.5s1.2 3.5 3 3.5c1 0 1.7-0.3 2.2-0.9C35.8 9.4 36 8.6 36 7.4 36 6.3 35.8 5.6 35.2 4.9M47.2 1.6L46 6.1c-0.1 0.6-0.2 1-0.3 1.5 -0.1-0.5-0.1-0.8-0.3-1.5l-1.2-4.6h-2.3l-0.9 9.1h1.8l0.3-4.8c0-0.6 0.1-1.1 0.1-1.6 0.1 0.5 0.2 1.2 0.4 1.6l1.3 4.8h1.6l1.4-5c0.2-0.6 0.2-0.9 0.3-1.4 0 0.5 0 0.9 0.1 1.5l0.3 4.9h1.8l-0.8-9.1C49.6 1.6 47.2 1.6 47.2 1.6zM55.6 6.5h-2v0c0-0.9 0.4-1.5 1-1.5 0.3 0 0.6 0.1 0.8 0.4 0.2 0.2 0.2 0.5 0.2 1L55.6 6.5 55.6 6.5zM54.6 3.8c-0.9 0-1.6 0.3-2.1 0.9 -0.6 0.7-0.8 1.5-0.8 2.7 0 2.1 1.2 3.5 3.2 3.5 0.9 0 1.8-0.3 2.5-0.9l-0.7-1c-0.6 0.4-1.1 0.7-1.7 0.7 -0.9 0-1.4-0.6-1.4-1.6V7.8h3.9V7.4c0-1.4-0.3-2.3-0.9-2.9C56.1 4 55.4 3.8 54.6 3.8M61.4 9.6c-0.6 0-0.8-0.2-0.8-1.1V5.1h1.1l0.4-1.2h-1.6v-2H59v2h-0.7v1.2h0.7v3.6c0 0.6 0 0.8 0.1 1 0.2 0.6 0.9 1 1.8 1 0.4 0 0.9-0.1 1.4-0.3l-0.2-1C61.8 9.5 61.6 9.6 61.4 9.6M66.6 9.1c-0.3 0.3-0.6 0.4-0.9 0.4 -0.4 0-0.8-0.3-0.8-0.9 0-0.8 0.4-1 1.6-1h0.1V9.1zM68.3 8.5V8.4l0-2.2c0-0.7 0-0.9-0.1-1.2 -0.3-0.8-1-1.2-2.2-1.2 -0.6 0-1.2 0.1-1.8 0.4 -0.5 0.2-0.7 0.3-1.1 0.6l0.8 1.2c0.7-0.5 1.4-0.8 1.9-0.8 0.7 0 0.8 0.2 0.8 1v0.3c-0.1 0-0.3 0-0.4 0 -2.1 0-3.2 0.7-3.2 2.3 0 1.3 0.8 2.1 2.3 2.1 0.6 0 1-0.1 1.3-0.4 0.1-0.1 0.3-0.2 0.4-0.3 0.2 0.3 0.6 0.7 0.9 0.8L69 9.9C68.4 9.5 68.3 9.2 68.3 8.5"/></svg>';
    }

    /**
     * Add defer attribute to all scripts
     */
    function add_defer_attribute($tag, $handle)
    {
        if ($handle != 'jquery') :
            return str_replace(' type', ' defer type', $tag);
        else :
            return $tag;
        endif;
    }


    function override_mce_options($initArray) {
        $opts = '*[*]';
        $initArray['valid_elements'] = $opts;
        $initArray['extended_valid_elements'] = $opts;
        return $initArray;
    }



    // Happy Theming !
}

new StarterSite();