!function(e){for(var n,o=function(){},i=["assert","clear","count","debug","dir","dirxml","error","exception","group","groupCollapsed","groupEnd","info","log","markTimeline","profile","profileEnd","table","time","timeEnd","timeline","timelineEnd","timeStamp","trace","warn"],r=i.length,l=e.console=e.console||{};r--;)l[n=i[r]]||(l[n]=o)}(window);
//# sourceMappingURL=../maps/utils/console.js.map
