!function(n){"use strict";window.autobind=function(o,i){i=assign({},i);var n=Object.getOwnPropertyNames(o.constructor.prototype);forEach(n,function(n){var t,c,e=o[n];"constructor"!==n&&"function"==typeof e&&(t=n,(c=i).include?-1<c.include.indexOf(t):!(c.exclude&&-1<c.exclude.indexOf(t)))&&(o[n]=e.bind(o))})}}();
//# sourceMappingURL=../maps/utils/autobind.js.map
