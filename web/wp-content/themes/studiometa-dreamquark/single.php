<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;


$args = array(
	'post_type'      => array( 'post' ),
	'posts_per_page' => '10', // Number of posts
	'order'          => 'DESC',
	'orderby'        => 'date',
	'post__not_in'        => array($post->ID)
);

$context['articles'] = Timber::get_posts( $args );


$custom_logo_id = get_theme_mod( 'custom_logo' );
$context['logo_url'] = wp_get_attachment_image_src( $custom_logo_id , 'full' )[0];

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
