<?php
/**
 * Spécifie le répertoire où seront sauvegardés les fichiers JSON de configuration d'ACF
 * Par convention, on utilisera un dossier 'config/' situé un niveau au-dessus du docRoot.
 * NOTE le dossier config a besoin d'être créer par vos soins
 */
if (! function_exists( 'meta_acf_json_save_point' ) ) {
	function meta_acf_json_save_point($path) {
		// update path
		$path = $_SERVER['DOCUMENT_ROOT'] . '/../config/';
		return $path;
	}

	add_filter( 'acf/settings/save_json', 'meta_acf_json_save_point' );
}

/**
 * Set google maps api key for map field
 */

if (!function_exists('studiometa_acf_gmap_init')) {
    function studiometa_acf_gmap_init( $api ){
        $api['key'] = GMAP_API_KEY;
        return $api;
    }
    add_filter('acf/fields/google_map/api', 'studiometa_acf_gmap_init');
}


/**
 * Importe les configurations sauvegardées
 */
if ( ! function_exists( 'meta_import_acf_config' ) ) {
	function meta_import_acf_config() {
		$path = $_SERVER['DOCUMENT_ROOT'] . '/../config/';
		$directory = new DirectoryIterator($path);

		foreach ($directory as $file) {
			if (!$file->isDot() && 'json' == $file->getExtension()) {
				$array = json_decode( file_get_contents( $file->getPathname() ), true );

				if (function_exists('acf_add_local_field_group')) {
					acf_add_local_field_group( $array );
				}
			}
		}
	}

	add_action( 'init', 'meta_import_acf_config' );
}

/**
 * Add a 'Very Simple' toolbar style for the WYSIWYG editor in ACF
 *  http://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/
 */
if ( ! function_exists( 'acf_wysiwyg_toolbar' ) ) {
	function acf_wysiwyg_toolbar( $toolbars ) {
		$toolbars['Text Based'] = array();
		// Only one row of buttons
		$toolbars['Text Based'][1] = array('formatselect', 'bold', 'italic', 'blockquote', 'bullist', 'numlist', 'link', 'unlink' );
		return $toolbars;
	}
	add_filter( 'acf/fields/wysiwyg/toolbars' , 'acf_wysiwyg_toolbar'  );
}

/**
 * Register Site wide ACF Options page
 * Doc: https://www.advancedcustomfields.com/resources/options-page/
 */
if ( function_exists( 'acf_add_options_page' )) {
	acf_add_options_page( 'Options' );
}

add_filter( 'acf/save_post', 'acf_clear_object_cache' );

/**
 * Intended to clear a post's cache
 */
function acf_clear_object_cache( $post_id ) {
    if ( empty( $_POST['acf'] ) ) {
        return;
    }

    // clear post related cache
    clean_post_cache( $post_id );

    // clear ACF cache
    if ( is_array( $_POST['acf'] ) ) {
        foreach ( $_POST['acf'] as $field_name => $value ) {
            $cache_slug = "load_value/post_id={$post_id}/name={$field_name}";
            wp_cache_delete( $cache_slug, 'acf' );
        }
    }
}
