<?php
/**
 * Studiometa WP Clean Up
 */
if ( ! function_exists( 'studiometa_start_cleanup' ) ) {
	function studiometa_start_cleanup() {
		// Launching operation cleanup.
		add_action( 'init', 'studiometa_cleanup_head' );
		// Remove WP version from RSS.
		add_filter( 'the_generator', 'studiometa_remove_version' );
		// Remove Wordpress version from js & css
		add_filter( 'style_loader_src', 'studiometa_remove_version_css_js', 9999 );
		// Remove login error message
		add_filter( 'login_errors', 'studiometa_no_wordpress_errors' );
		// remove emoji stuff
		add_action( 'init', 'studiometa_disable_wp_emojicons' );
	}
}

/**
 * Clean up all the useless stuff added in <head> by Wordpress
 */
if ( ! function_exists( 'studiometa_cleanup_head' ) ) {
	function studiometa_cleanup_head() {
		// EditURI link.
		remove_action( 'wp_head', 'rsd_link' );
		// Category feed links.
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		// Post and comment feed links.
		remove_action( 'wp_head', 'feed_links', 2 );
		// Windows Live Writer.
		remove_action( 'wp_head', 'wlwmanifest_link' );
		// Index link.
		remove_action( 'wp_head', 'index_rel_link' );
		// Previous link.
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		// Start link.
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		// Canonical.
		remove_action( 'wp_head', 'rel_canonical', 10, 0 );
		// Shortlink.
		remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
		// Links for adjacent posts.
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
		// WP version.
		remove_action( 'wp_head', 'wp_generator' );
	}
}

/**
 * Supprime la version de WordPress dans la balise meta generator
 */
if ( ! function_exists( 'studiometa_remove_version' ) ) {
	function studiometa_remove_version() {
		return '';
	}
}

/**
 * Supprime la version de WordPress dans les appels CSS/JS
 */
if ( ! function_exists( 'studiometa_remove_version_css_js' ) ) {
	function studiometa_remove_version_css_js($src) {
		if ( strpos( $src, 'ver=' ) )
			$src = remove_query_arg( 'ver', $src );
		return $src;
	}
}

/**
 * Désactive les messages d’erreur de connexion
 */
if ( ! function_exists( 'studiometa_no_wordpress_errors' ) ) {
	function studiometa_no_wordpress_errors() {
		return 'Something is wrong!';
	}
}

/**
 * Disable emojis
 */
if ( ! function_exists( 'studiometa_disable_wp_emojicons' ) ) {
	function studiometa_disable_wp_emojicons() {
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	}
}
