<?php
/*
 * Register all custom post types here
 * Doc : https://codex.wordpress.org/Function_Reference/register_post_type
 */

// Register Use Cases Custom Post Type

$labels_uc = array(
    'name'                  => _x( 'Use Cases', 'Post Type General Name', 'dreamquark' ),
    'singular_name'         => _x( 'Use Case', 'Post Type Singular Name', 'dreamquark' ),
    'menu_name'             => __( 'Use Cases', 'dreamquark' ),
    'name_admin_bar'        => __( 'Use Case', 'dreamquark' ),
    'archives'              => __( 'Use Cases Archives', 'dreamquark' ),
    'attributes'            => __( 'Item Attributes', 'dreamquark' ),
    'parent_item_colon'     => __( 'Parent case:', 'dreamquark' ),
    'all_items'             => __( 'All Cases', 'dreamquark' ),
    'add_new_item'          => __( 'Add New Case', 'dreamquark' ),
    'add_new'               => __( 'Add New', 'dreamquark' ),
    'new_item'              => __( 'New Case', 'dreamquark' ),
    'edit_item'             => __( 'Edit Case', 'dreamquark' ),
    'update_item'           => __( 'Update Case', 'dreamquark' ),
    'view_item'             => __( 'View Case', 'dreamquark' ),
    'view_items'            => __( 'View Cases', 'dreamquark' ),
    'search_items'          => __( 'Search Case', 'dreamquark' ),
    'not_found'             => __( 'Not found', 'dreamquark' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'dreamquark' ),
    'featured_image'        => __( 'Featured Image', 'dreamquark' ),
    'set_featured_image'    => __( 'Set featured image', 'dreamquark' ),
    'remove_featured_image' => __( 'Remove featured image', 'dreamquark' ),
    'use_featured_image'    => __( 'Use as featured image', 'dreamquark' ),
    'insert_into_item'      => __( 'Insert into item', 'dreamquark' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'dreamquark' ),
    'items_list'            => __( 'Items list', 'dreamquark' ),
    'items_list_navigation' => __( 'Items list navigation', 'dreamquark' ),
    'filter_items_list'     => __( 'Filter items list', 'dreamquark' ),
);
$args_uc = array(
    'label'                 => __( 'Use Case', 'dreamquark' ),
    'labels'                => $labels_uc,
    'supports'              => array( 'title', 'editor', 'thumbnail' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-clipboard',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'show_in_rest'          => true,
);


$labels_team = array(
    'name'                  => _x( 'Team', 'Post Type General Name', 'dreamquark' ),
    'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'dreamquark' ),
    'menu_name'             => __( 'Team', 'dreamquark' ),
    'name_admin_bar'        => __( 'Team', 'dreamquark' ),
    'archives'              => __( 'Team Archives', 'dreamquark' ),
    'attributes'            => __( 'Item Attributes', 'dreamquark' ),
    'parent_item_colon'     => __( 'Parent case:', 'dreamquark' ),
    'all_items'             => __( 'All Persons', 'dreamquark' ),
    'add_new_item'          => __( 'Add New Person', 'dreamquark' ),
    'add_new'               => __( 'Add New', 'dreamquark' ),
    'new_item'              => __( 'New Person', 'dreamquark' ),
    'edit_item'             => __( 'Edit Person', 'dreamquark' ),
    'update_item'           => __( 'Update Person', 'dreamquark' ),
    'view_item'             => __( 'View Person', 'dreamquark' ),
    'view_items'            => __( 'View Persons', 'dreamquark' ),
    'search_items'          => __( 'Search Person', 'dreamquark' ),
    'not_found'             => __( 'Not found', 'dreamquark' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'dreamquark' ),
    'featured_image'        => __( 'Featured Image', 'dreamquark' ),
    'set_featured_image'    => __( 'Set featured image', 'dreamquark' ),
    'remove_featured_image' => __( 'Remove featured image', 'dreamquark' ),
    'use_featured_image'    => __( 'Use as featured image', 'dreamquark' ),
    'insert_into_item'      => __( 'Insert into item', 'dreamquark' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'dreamquark' ),
    'items_list'            => __( 'Items list', 'dreamquark' ),
    'items_list_navigation' => __( 'Items list navigation', 'dreamquark' ),
    'filter_items_list'     => __( 'Filter items list', 'dreamquark' ),
);

$args_team = array(
    'label'                 => __( 'Team', 'dreamquark' ),
    'labels'                => $labels_team,
    'supports'              => array( 'title', 'editor', 'thumbnail' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-groups',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'show_in_rest'          => true,
);

register_post_type( 'dq_use_case', $args_uc );
register_post_type( 'dq_team', $args_team );

