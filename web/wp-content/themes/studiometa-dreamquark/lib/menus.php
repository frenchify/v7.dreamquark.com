<?php
/*
 * Register Menus
 * Doc : https://codex.wordpress.org/Function_Reference/register_nav_menus
 */

register_nav_menu( 'main-menu', __( 'Main menu' ) );
register_nav_menu( 'footer-main-menu', __( 'Footer Main menu' ) );
register_nav_menu( 'footer-sub-menu', __( 'Footer Sub menu' ) );
