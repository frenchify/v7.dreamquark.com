<?php
/**
 * Set product
 * Doc: https://timber.github.io/docs/guides/woocommerce/
 *
 */
function timber_set_product( $post ) {
	global $product;

	if ( is_woocommerce() ) {
		$product = wc_get_product( $post->ID );
	}
}
