<?php

// jQuery
wp_deregister_script('jquery');
wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js', array(), null, false);
wp_enqueue_script('jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js', array(), null, false);

// Components
if (WP_ENV == 'PROD') {
    $js_directory = get_template_directory_uri().'/dist/scripts';
} else {
    $js_directory = get_template_directory_uri().'/dist/scripts';
}

wp_register_script('logger', $js_directory . '/utils/logger.js', array(), null, true);
wp_register_script('debounce', $js_directory . '/utils/debounce.js', array(), null, true);
wp_register_script('viewport', $js_directory . '/utils/viewport.js', ['debounce'], null, true);
wp_register_script('logger', $js_directory . '/utils/logger.js', array(), null, true);


// Vendors
// GSAP
wp_register_script('tween-lite', $js_directory.'/vendors/TweenLite.min.js', 9, false, true);
wp_register_script('tween-max', $js_directory.'/vendors/TweenMax.min.js', 9, false, true);
wp_register_script('css-plugin', $js_directory.'/vendors/CSSPlugin.min.js', 9);
wp_register_script('ease-pack', $js_directory.'/vendors/EasePack.min.js', 9);
wp_register_script('scroll-to-plugin', $js_directory.'/vendors/ScrollToPlugin.min.js', 9);
wp_register_script('morph-svg-plugin', $js_directory.'/vendors/MorphSVGPlugin.min.js', 9);
wp_register_script('custom-ease', $js_directory.'/vendors/CustomEase.min.js', 10);

// Vue
wp_register_script('vue', $js_directory.'/vendors/vue.js', 9);
//GMAP
wp_register_script('gmap', 'https://maps.googleapis.com/maps/api/js?key='.GMAP_API_KEY , 9);
//Require JS

//Blazy.js
wp_register_script('blazy', $js_directory.'/vendors/blazy.min.js', 9);

wp_register_script('app', $js_directory . '/app.js', [
    'tween-max',
    'blazy',
    'logger',
    'viewport',
], null, true);

wp_enqueue_script('app');
