<?php
/**
 * Theme Support
 * Doc : https://codex.wordpress.org/Function_Reference/get_theme_support
 */


/* ================================
 * Add post thumbnail Support
 * Doc: http://codex.wordpress.org/Post_Thumbnails
 * ================================ */
add_theme_support( 'post-thumbnails' );


/* ================================
 * Add menu support
 * ================================ */
add_theme_support( 'menus' );

/* ================================
 * Add post formats support
 * Doc: http://codex.wordpress.org/Post_Formats
 * ================================ */
add_theme_support( 'post-formats' );


/* ================================
 * Custom Image sizes
 * Doc : https://developer.wordpress.org/reference/functions/add_image_size/
 * ================================ */
// add_image_size( string $name, int $width, int $height, bool|array $crop = false )
// add_image_size( 'sticky', 470, 336, array('center', 'center') );


/* ================================
 * WooCommerce support
 * Doc: http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
 * ================================ */
if (THEME_USE_WOOCOMMERCE) {
	add_theme_support( 'woocommerce' );
}
