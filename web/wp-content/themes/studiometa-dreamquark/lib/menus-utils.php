<?php
/**
 * [studiometa_nav_class description]
 * @param  [type] $classes [description]
 * @param  [type] $item    [description]
 * @return [type]          [description]
 */
if (! function_exists('studiometa_nav_class')) {
	function studiometa_nav_class( $classes, $item ) {
		$new_classes = array();
		$base_class = 'menu__item';

		// Default class
		$new_classes[] = $base_class;

		// Active item class
		$item->current ? $new_classes[] = $base_class . '--active' : '';

		// Type class (post, page....)
		$new_classes[] = $base_class . '--' . $item->object;

		// Add class from item->ID
		// $new_classes[] = $base_class . '--' . $item->ID;

		// Add class if menu item is in a sublevel
		$item->menu_item_parent && $item->menu_item_parent > 0 ? $new_classes[] = $base_class . '--sub' : '';

		return $new_classes;
	}

	add_filter( 'nav_menu_css_class', 'studiometa_nav_class', 10, 2 );
}
