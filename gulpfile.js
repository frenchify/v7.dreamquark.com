'use strict';

// Global vars
global.HOST = `local.dreamquark`;
global.DOCROOT = `web/`;
global.THEME_PATH = `${DOCROOT}wp-content/themes/studiometa-dreamquark/`;
global.SRC_PATH = `${DOCROOT}wp-content/themes/studiometa-dreamquark/src/`;
global.DIST_PATH = `${DOCROOT}wp-content/themes/studiometa-dreamquark/dist/`;

// Notify options
global.NOTIFY_OPTIONS = {
	contentImage: `${DOCROOT}android-chrome-96x96.png`,
	sound: false,
};

// Is not watching by default
global.IS_WATCHING = false;
global.IS_SERVING = false;

global.LOGS = [
	[`' '`],
	[
		`'%c Made with <3 by Studio Meta %c www.studiometa.fr ', 'background: #333; color: #fff; font-weight: 500;padding: 5px;', 'background: #eee; color: #333; font-weight: 500;padding: 5px;'`,
	],
	[`' '`],
];

// Code to prepend to the bundled javascript file
global.PREPEND = `/*!************************************************************************************
 Made with love by the fantastic, the incredible, the amazing...
**************************************************************************************

         888                  888 d8b                                 888
         888                  888 Y8P                                 888
         888                  888                                     888
.d8888b  888888 888  888  .d88888 888  .d88b.  88888b.d88b.   .d88b.  888888  8888b.
88K      888    888  888 d88" 888 888 d88""88b 888 "888 "88b d8P  Y8b 888        "88b
"Y8888b. 888    888  888 888  888 888 888  888 888  888  888 88888888 888    .d888888
     X88 Y88b.  Y88b 888 Y88b 888 888 Y88..88P 888  888  888 Y8b.     Y88b.  888  888
 88888P'  "Y888  "Y88888  "Y88888 888  "Y88P"  888  888  888  "Y8888   "Y888 "Y888888

*************************************************************************************
 Interactive Design Agency - http://www.studiometa.fr   <info@studiometa.fr>
*************************************************************************************/
`;

require('./gulp');
