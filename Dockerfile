# docker/node/Dockerfile
# See https://github.com/nodejs/docker-node#dockerfile
FROM node:6

EXPOSE 3000

# set the working directory
RUN mkdir /app
COPY . /app
WORKDIR /app

# delete existing modules and re-install dependencies
ENV NODE_ENV=dev
RUN rm -rf node_modules
RUN npm install --only=dev
RUN npm install gulp-cli -g
 
ENV PATH="${PATH}:./node_modules/.bin"